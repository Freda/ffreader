package fanfictionreader.ffnetreader.services;

public class FFNetMetaDataIndex {
	final static int RATING_INDEX = 0; 
	final static int LANGUAGE_INDEX = 1; 
	final static int GENRE_INDEX = 2; 
	final static int CHARACTER_INDEX = 3; 
	final static int CHAPTER_INDEX = 4; 
	final static int WORD_COUNT_INDEX = 5; 
	final static int REVIEW_COUNT_INDEX = 6; 
	final static int FAV_COUNT_INDEX = 7; 
	final static int FOLLOW_COUNT_INDEX = 8; 
	final static int UPDATE_INDEX = 10; 
	final static int PUBLISHED_INDEX = 12; 
	final static int STATUS_INDEX = 13; 
	final static int STORY_ID_INDEX = 14; 
}
