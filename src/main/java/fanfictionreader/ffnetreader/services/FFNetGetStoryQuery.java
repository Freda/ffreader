package fanfictionreader.ffnetreader.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class FFNetGetStoryQuery {
	//TODO What happens if can't load website, parsing fails.....

	private static String storyLink = "https://www.fanfiction.net/s/";
	
	public static void getStory(String storyId) throws Exception{
		Document fanfic = getJsoupDoc(storyId); 
		getStoryStatData(fanfic); 
		getStoryMetaData(fanfic);
		getStoryTextData(storyId, 3);  //TODO Figure out how I am storing data and how to get num chapters
		
//		getStoryTitle(); 
//		getStoryArchCat(); 
//		getStorySubCat(); 
	}
	
	private static Document getJsoupDoc(String storyId) throws Exception{
		String curStoryLink = storyLink + storyId;
		Document meetingDoc = Jsoup.connect(curStoryLink).get();
		return meetingDoc; 
	}
	
	public static void getStoryStatData(Document fanfic) throws Exception{
		String[] allData = getStoryData(fanfic); 
		getStoryNumChapters(allData); 
		getStoryNumWords(allData);
		getStoryNumReviews(allData);
		getStoryNumFavs(allData);
		getStoryNumFollows(allData);
	}
	
	private static String[] getStoryData(Document fanfic) throws Exception{
		Elements allSData = fanfic.select("div.xcontrast_outer");
		allSData = allSData.select("div.xcontrast.maxwidth");
		allSData = allSData.select("div#content_wrapper_inner"); 
		allSData = allSData.select("div#profile_top");
		allSData = allSData.select("span.xgray.xcontrast_txt");
		return allSData.toString().split("-");
	}
	
	private static void getStoryNumChapters(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.CHAPTER_INDEX];
		System.out.println(chapters.split(" ")[2]); 
	}

	private static void getStoryNumWords(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.WORD_COUNT_INDEX];
		System.out.println(chapters.split(" ")[2]); 
	}

	private static void getStoryNumReviews(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.REVIEW_COUNT_INDEX];
		System.out.println(chapters.split(">")[1].split("<")[0]); 
	}

	private static void getStoryNumFavs(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.FAV_COUNT_INDEX];
		System.out.println(chapters.split(" ")[2]); 
	}

	private static void getStoryNumFollows(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.FOLLOW_COUNT_INDEX];
		System.out.println(chapters.split(" ")[2]); 
	}

	public static void getStoryMetaData(Document fanfic) throws Exception{
		String[] allData = getStoryData(fanfic); 
		getStoryGenre(allData); 
		getStoryRating(allData); 
		getStoryLanguage(allData);  
		getStoryCharacters(allData); 
		getStoryStatus(allData); 
	}
	
	private static void getStoryGenre(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.GENRE_INDEX];
		System.out.println(chapters.split(" ")[1]); 
	}

	private static void getStoryRating(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.RATING_INDEX];
		System.out.println(chapters.split(">")[2].split("<")[0]); 
	}

	private static void getStoryLanguage(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.LANGUAGE_INDEX];
		System.out.println(chapters.split(" ")[1]); 
	}

	private static void getStoryCharacters(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.CHARACTER_INDEX];
		System.out.println(chapters.trim()); 
	}

	private static void getStoryStatus(String[] allData) {
		String chapters = allData[FFNetMetaDataIndex.STATUS_INDEX];
		System.out.println(chapters.split(" ")[2]); 
	}

	private static void getStoryTextData(String storyId, int numChapters) throws Exception{
		getStoryTextData(storyId, 1, numChapters); 
	}
	
	public static void getStoryTextData(String storyId, int start, int end) throws Exception{
		for (int i = start; i <= end; i++){
			Document fanfic = getJsoupDoc(storyId + "/" + i);
			String divText = getChapterTextDiv(fanfic);
			getChapterText(divText);
		} 
	}
	
	private static String getChapterTextDiv(Document fanfic) throws Exception{
		Elements allSData = fanfic.select("div.xcontrast_outer");
		allSData = allSData.select("div.xcontrast.maxwidth");
		allSData = allSData.select("div#content_wrapper_inner");
		allSData = allSData.select("div.storytextp");
		allSData = allSData.select("div.storytext.xcontrast_txt.nocopy");
		return allSData.toString();
	}
	
	private static String getChapterText(String divData) throws Exception{
		String text = divData.replace("<div class=\"storytext xcontrast_txt nocopy\" id=\"storytext\">","");
		text = text.replace("</div>", "");
		return text; 
	}
}
