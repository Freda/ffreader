package fanfictionreader.ffnetreader.resources;

import javax.ws.rs.*;
import fanfictionreader.ffnetreader.services.FFNetQuery;
//import javax.xml.bind.JAXBContext;

@Path("/")
public class FFNetResources {

	@GET
	@Path("/helloworld")
	@Produces("text/html")
	public String helloWorld() throws Exception {
		return "Hello World";
	}
	
	@GET
	@Path("/s/{storyId}")
	@Produces("text/html")
	public String getNewStory(@PathParam("storyId") String storyId) throws Exception {
		String fullStory = FFNetQuery.getNewStory(storyId); 
		return fullStory;
	}
}
