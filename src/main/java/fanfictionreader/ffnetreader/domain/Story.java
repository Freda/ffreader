package fanfictionreader.ffnetreader.domain;

public class Story {
	
	//make class storyStatData
	private int numChapters; 
	private int numWords; 
	private int numReviews;
	private int numFavs;
	private int numFollows;
	
	//make class storyDateData
	private String published; //use Java Date
	private String updated;   //use Java Date
	
	//make class storyMetaData
	private String title; 
	private int storyId; 
	private String author; 
	private int authorId; 
	private String genre; 
	private String rating;   //enum
	private String language; //enum
	private String archCategory; //enum
	private String subCategory; 
	private String characters;
	
	public int getNumChapters() {
		return numChapters;
	}
	public void setNumChapters(int numChapters) {
		this.numChapters = numChapters;
	}
	public int getNumWords() {
		return numWords;
	}
	public void setNumWords(int numWords) {
		this.numWords = numWords;
	}
	public int getNumReviews() {
		return numReviews;
	}
	public void setNumReviews(int numReviews) {
		this.numReviews = numReviews;
	}
	public int getNumFavs() {
		return numFavs;
	}
	public void setNumFavs(int numFavs) {
		this.numFavs = numFavs;
	}
	public int getNumFollows() {
		return numFollows;
	}
	public void setNumFollows(int numFollows) {
		this.numFollows = numFollows;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getStoryId() {
		return storyId;
	}
	public void setStoryId(int storyId) {
		this.storyId = storyId;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getAuthorId() {
		return authorId;
	}
	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getPublished() {
		return published;
	}
	public void setPublished(String published) {
		this.published = published;
	}
	public String getUpdated() {
		return updated;
	}
	public void setUpdated(String updated) {
		this.updated = updated;
	}
	public String getArchCategory() {
		return archCategory;
	}
	public void setArchCategory(String archCategory) {
		this.archCategory = archCategory;
	}
	public String getSubCategory() {
		return subCategory;
	}
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	public String getCharacters() {
		return characters;
	}
	public void setCharacters(String characters) {
		this.characters = characters;
	} 
}
