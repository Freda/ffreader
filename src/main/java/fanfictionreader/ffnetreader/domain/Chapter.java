package fanfictionreader.ffnetreader.domain;

public class Chapter {
	
	private String chapterText; 
	private int chapterNum; 
	private int numWords; 
	private int storyId;
	
	public String getChapterText() {
		return chapterText;
	}
	public void setChapterText(String chapterText) {
		this.chapterText = chapterText;
	}
	public int getChapterNum() {
		return chapterNum;
	}
	public void setChapterNum(int chapterNum) {
		this.chapterNum = chapterNum;
	}
	public int getNumWords() {
		return numWords;
	}
	public void setNumWords(int numWords) {
		this.numWords = numWords;
	}
	public int getStoryId() {
		return storyId;
	}
	public void setStoryId(int storyId) {
		this.storyId = storyId;
	} 
	

}
